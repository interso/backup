#!/bin/bash
       
ERRORLIST=''
SUBYSTEM=''

rm_log(){
    rm -f "${LOGFILE}${SUBSYSTEM}"
}

add_log(){
# $1 - message
    echo -en "$1">>"${LOGFILE}${SUBSYSTEM}"
    [ "$VERBOSE" != 'no' ] && echo -en "$1"
}

debug(){
# $1 - message
   [ "$VERBOSE" == 'debug' ] && add_log "$1"
}

send_mail(){
    if [ "$EMAIL" ]; then
        mail -s "backup report on $HOST ($1)" "$EMAIL" <"$LOGFILE"
    else
        echo -e "backup report on $HOST ($1)\n"
        cat "$LOGFILE"
    fi
}

failed(){
# $1 - error code
    add_log "FAILED ($1)\n"
    ERRORLIST="$ERRORLIST $1"
}

fatal_error(){
# $1 - error message
# $2 - error code
    SUBSYSTEM=''
    add_log "\nFATAL: $1\nbackup aborted!\n"
    send_mail "FATAL ERROR ($2)"
    exit $2
}

backup_fs(){
  local count=0
  local error=0
  SUBSYSTEM='fs'

  mkdir -p $BACKUP_DIR/$SUBSYSTEM
  if [ "$backuptype" == "inc" ]; then

    if [ ! -d "$BACKUP_DIR/$SUBSYSTEM" ]; then
      backuptype="full"
    else	    
      LAST_BACKUP=`ls -1 $BACKUP_DIR/$SUBSYSTEM | tail -n 1`
      if [ "$LAST_BACKUP" == "" ]; then
        backuptype="full"
      else
        date=`basename $LAST_BACKUP`                   
        BDIR="$BACKUP_DIR/$SUBSYSTEM/$date"
        [ -d $BDIR ] || backuptype="full"
        [ -d "$BDIR/inc" ] || backuptype="full"
      fi; 
    fi;    
  fi;

  if [ "$backuptype" = "full" ]; then
    date=`date +'%Y-%m-%d'`
    BDIR="$BACKUP_DIR/$SUBSYSTEM/$date"
    [ ! -d $BDIR ] || fatal_error "\tdirectory for full file backup exists: $BDIR\n" 35
    mkdir -p "$BDIR" || fatal_error "can't create dir '$BDIR'" 113
    mkdir -p "$BDIR/inc" || fatal_error "can't create dir '$BDIR/increment'" 113
    
    cleanup $BACKUP_DIR/$SUBSYSTEM $FS_CYCLES
  fi;

  add_log "\nbackup $backuptype fs ($FS_DIRS)...\n"

  zstd_path=`which zstd`
  if [ "$zstd_path" != "" ]; then
     compress="zstd"
     ext="tar.zst"
  else
     compress="gzip"
     ext="tar.gz"
  fi;         


  for DIR in $FS_DIRS; do
    add_log "(fs) Backup $DIR..."
    [ -d "$DIR" ] || { add_log "\tSKIPPED: not a dir '$DIR'\n"; continue; }
    [ ! -h "$DIR" ] || { echo -e "\tSKIPPED: symlink '$DIR'\n"; continue; }

    fname=`echo "$DIR"|sed 's/^\///; s/\/$//; s/\//-/g'`
    dir_name=`basename "$DIR"`
    change_dir=`dirname "$DIR"`
    datefile=`date +'%Y-%m-%d_%H%M%S'`

    if [ "$backuptype" = 'inc' ]; then
      archive_file="$BDIR/inc/$fname-$datefile.$ext"
    else
      archive_file="$BDIR/$fname-$datefile.$ext"
    fi 

    snar_file="$BDIR/$fname.snar"
    taropt="--create --ignore-failed-read --use-compress-program=$compress --listed-incremental=$snar_file --directory=$change_dir --file=$archive_file $dir_name"

    tar $taropt 2>/dev/null
    
    # if [ $? -eq 0 ]; then
    # tar change in 1.16: and --ignore-failed-read return error code = 1

    if [ $? -eq 0 -o $? -eq 1 ]; then
      add_log "\tDONE\n"
      let count=$count+1
    else
      add_log "\tFAILED ($?)\n"
      error=13
    fi

    debug "Call tar $taropt\n"
  done
  
  [ $error -ne 0 -o $VERBOSE != 'no' ] &&  cat "$LOGFILE$SUBSYSTEM">>"$LOGFILE"  
  rm_log

  SUBSYSTEM=''
  add_log "backup $SUBSYSTEM done ($count items total), status: $error\n"
  return $error
}

backup_mysql(){
  # dump all local mysql databases
  # user & passwd must be set in ~/.my.cnf
  SUBSYSTEM='mysql'
  date=`date +'%Y-%m-%d'`
  BDIR="$BACKUP_DIR/mysql/$date"
         
  mkdir -p $BACKUP_DIR/$SUBSYSTEM
 
  LAST_BACKUP_DATE=`ls -1 $BACKUP_DIR/mysql/ | tail --lines 1`
  if [ "$LAST_BACKUP_DATE" == "" ]; then
      LAST_BDIR=""
  else
      LAST_BACKUP_DATE=`basename $LAST_BACKUP_DATE`
      LAST_BDIR="$BACKUP_DIR/mysql/$LAST_BACKUP_DATE"
  fi;

  add_log "\nbackup $SUBSYSTEM (full) -> $BDIR...\n"

  [ ! -d $BDIR ] || fatal_error "\tdirectory for databases dumping exists: $BDIR\n" 35
  mkdir -p "$BDIR" || fatal_error "can't create dir '$BDIR'" 113
          
  local error=0
  local count=0
       
  opt_passwd=""
  if [ "$MYSQL_ROOT_PASSWORD" != "" ]; then
     opt_passwd=" -p$MYSQL_ROOT_PASSWORD "
  fi;

  docker_cmd=""
  if [ "$MYSQL_DOCKER_COMPOSE_DIR" != "" -a "$MYSQL_DOCKER_COMPOSE_SERVICE" != "" -a -d "$MYSQL_DOCKER_COMPOSE_DIR" -a -r "$MYSQL_DOCKER_COMPOSE_DIR/$MYSQL_DOCKER_COMPOSE_FILE" ]; then
     docker_cmd="docker-compose --log-level ERROR --project-directory $MYSQL_DOCKER_COMPOSE_DIR -f $MYSQL_DOCKER_COMPOSE_DIR/$MYSQL_DOCKER_COMPOSE_FILE exec  -T $MYSQL_DOCKER_COMPOSE_SERVICE /bin/bash"
  fi;

  add_log "(mysql) Get databases list for dumping... \n"

  DATABASES_CMD="echo 'show databases' | mysql -B --skip-column-names $opt_passwd | grep -v -i 'Warning:' | grep -v -P '$MYSQL_SKIP_DATABASE_REGEX'"

  if [ "$docker_cmd" != "" ]; then
     DATABASES_CMD="$docker_cmd -c \"$DATABASES_CMD\""
  fi;

  # echo $DATABASES_CMD
  DATABASES=$( echo $DATABASES_CMD | bash 2>/dev/null)

  zstd_path=`which zstd`
  za_path=`which 7za`
  if [ "$zstd_path" != "" ]; then

     zstd_have_no_progress=`zstd --help | grep no-progress`
     zstd_have_threads=`zstd --help | grep threads`

     compress="zstd -11 "
     if [ "$zstd_have_no_progress" != "" ]; then
         compress="$compress --no-progress "
     fi;
     if [ "$zstd_have_threads" != "" ]; then
         compress="$compress --threads=0 "
     fi;

     compress="$compress - -o "
     ext="mysql.zst"
  elif [ "$za_path" != "" ]; then
     compress="7za a  -t7z -m0=lzma2 -mx=4 -ms=off -mf=off -mmt=8 -si -bd "
     ext="mysql.7z"
  else 
     fatal_error "Not found compression zstd or 7za" 120
  fi;         

  for DATABASE in $DATABASES; do

    DUMP_CMD="mysqldump --opt --single-transaction $opt_passwd $DATABASE 2>/dev/null"
    if [ "$docker_cmd" != "" ]; then
       DUMP_CMD="$docker_cmd -c \"$DUMP_CMD\""
    fi;

    add_log "(mysql) Dumping $DATABASE..."

    $( echo $DUMP_CMD | bash | $compress $BDIR/$DATABASE.$ext >/dev/null 2>/dev/null)

    if [ $? -eq 0 ]; then
      add_log "\tDONE\n"
      let count=$count+1
    else
      add_log "\tFAILED\n"
      error=33
    fi
    
    debug "$DUMP_CMD"
  done;
  [ $error -ne 0 -o $VERBOSE != 'no' ] &&  cat "$LOGFILE$SUBSYSTEM">>"$LOGFILE"  
  rm_log 

  cleanup $BACKUP_DIR/$SUBSYSTEM $MYSQL_CYCLES


  add_log "backup $SUBSYSTEM done ($count items total), status: $error\n"
  SUBSYSTEM=''
  return $error
}

# Вызываем после полного бэкапа. удаляет старые бэкапы

cleanup() {

  local data_dir=$1
  local cycle=$2
  local qnt_cycle=`ls -1 $data_dir | wc -l`

  add_log "\nCleanup. Data dir:$data_dir Cycle:$qnt_cycle/$cycle\n"
  if [ $qnt_cycle -gt $cycle ]; then
    let remove_qnt=$qnt_cycle-$cycle
    builtin pushd "$data_dir"

    REMOVE_FILES=`ls -1 $data_dir | head -n $remove_qnt`
    for REMOVE_FILE in $REMOVE_FILES; do
      echo "rm -rf $data_dir$REMOVE_FILE"
      rm -rf "$data_dir/$REMOVE_FILE"
    done;
    builtin popd >/dev/null
  fi;
}

