#!/bin/bash

BASE_DIR=$(cd $(dirname $0) && pwd)
TARGET_FILE="$1"

if [ "$TARGET_FILE" == "" ]; then
   TARGET_FILE="/etc/cron.d/backup"
fi;

echo "" > $TARGET_FILE
echo "05 3 * * 0   root  $BASE_DIR/backup.sh full" >> $TARGET_FILE
echo "05 3 * * 1-6 root  $BASE_DIR/backup.sh inc"  >> $TARGET_FILE

#echo "#05 10 * * 0 root  /srv/backup/backup_cleanup.sh

