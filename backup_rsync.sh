#!/bin/bash

BASE_DIR=$(cd $(dirname $0) && pwd)

ENVFILE="$BASE_DIR/.env"
ENVFILE_LOCAL="$BASE_DIR/.env.local"

if [ ! -r $ENVFILE ]; then
   echo "Not found env file: $ENVFILE"
   exit 1
fi;

. $ENVFILE

if [ -r $ENVFILE_LOCAL ]; then
   . $ENVFILE_LOCAL
fi;

if [ "$BACKUP_DIR" == "" ]; then
   BACKUP_DIR="$BASE_DIR/data"
fi;

if [ ! -d $BACKUP_DIR ]; then
   echo "Not found backup directory: $BACKUP_DIR"
   exit 2
fi;

if [ "$REMOTE_RUN" != "yes" ]; then
  exit
fi;

REMOTE_KEY_FILE="$BASE_DIR/keys/$REMOTE_KEY"

DEST="$REMOTE_HOST:$REMOTE_BACKUPDIR/$HOST"
SRC="$BACKUP_DIR/"
           
rsync -e "ssh -l $REMOTE_LOGIN -i $REMOTE_KEY_FILE" --delete -auxS $SRC $DEST

