#!/bin/bash

PATH=$PATH:/usr/local/bin:/usr/local/sbin
umask 077

BASE_DIR=$(cd $(dirname $0) && pwd)

ENVFILE="$BASE_DIR/.env"
ENVFILE_LOCAL="$BASE_DIR/.env.local"

if [ ! -r $ENVFILE ]; then
   echo "Not found env file: $ENVFILE"
   exit 1
fi;

. $ENVFILE

if [ -r $ENVFILE_LOCAL ]; then
   . $ENVFILE_LOCAL	
fi;

if [ "$BACKUP_DIR" == "" ]; then
   BACKUP_DIR="$BASE_DIR/data"
fi;


if [ ! -d $BACKUP_DIR ]; then
   echo "Not found backup directory: $BACKUP_DIR"
   exit 2
fi;

. $BASE_DIR/backup.lib.sh || { echo "no backup library!"; exit 3; }

LOGFILE=`mktemp /tmp/backup.XXXXXX`
[ "$LOGFILE" ] || { echo "can't create logfile!"; exit 3; }

if [ "$1" == 'full' ]; then
    backuptype='full'
else    
    backuptype='inc'
fi

add_log "Starting $backuptype backup...\n"

if [ "$FS_RUN" == "yes" ]; then 
  backup_fs || failed $?
fi;

if [ "$MYSQL_RUN" == "yes" ]; then
  backup_mysql || failed $?
fi;

if [ "$CHANGE_OWNER" == "yes" ]; then
  chown -R $BACKUP_UID:$BACKUP_GID $BACKUP_DIR
fi;

if [ "$REMOTE_RUN" == "yes" ]; then
  $BASE_DIR/backup_rsync.sh
fi;

if [ $ERRORLIST ]; then
    add_log "\nall done status:FAILED ($ERRORLIST)\n"
    send_mail "FAILED"
    rm_log
    exit 99
else
    add_log "\nall done status:all ok ($ERRORLIST)\n"
    send_mail "all ok"
    rm_log
fi


